<?php include('scaffold.php'); ?>
<?php print $top; ?>

<div id="ticketdetail">

    <div class="row ticket">
        <div class="col s8 white left">
            <div class="subject-and-date">
                <div class="subject left">Neue Filteransicht für Shop benötigt
                </div>
                <div class="date right">am 27.10.2018 - 15:16 Uhr</div>
            </div>
            <div class="mail-receiver">
                <div class="from">Von: py.valeris@oy-vymo.com</div>
                <div class="cc">CC: py.valeris@oy-vymo.com</div>
            </div>

            <div class="message flow-text">
                Sehr geehrter Herr Sowieso
                <br> <br> Gerne möchte ich auf Sie zu kommen, da es nicht
                ganz klar
                ist
                wie und was genau funktioniert.
                <br>
                Haben Sie bereits ein Auge auf die Ansicht für den Shop
                werden
                können? Es wäre gut wenn Weit hinten, hinter den Wortbergen,
                fern der Länder kalien und Konsonantien leben die
                Blindtexte.
                Abgeschieden wohnen sie in Buchstabhausen an der Küste des
                Semantik, eines großen Sprachozeans.
                <br> <br>
                Ein kleines Bächlein namens Duden fließt durch ihren Ort und
                versorgt sie mit den nötigen Regelialien. Es ist ein
                paradiesmatisches Land, in dem einem gebratene Satzteile in
                den
                Mund fliegen. <br> <br>
                Nicht einmal von der allmächtigen
                Interpunktion
                werden die Blindtexte beherrscht – ein geradezu
                unorthographisches Leben. Eines Tages aber beschloß eine
                kleine
                Zeile Blindtext, ihr Name war Lorem Ipsum, hinaus zu gehen
                in
                die weite Grammatik.<br> <br>


                Beste Grüsse <br>
                Py Valeris Meier<br> <br>

                Py Vymo  <br>
                +487856446 <br>
                Meierstrasse 99 <br>
                www.py-vymo.com
            </div>
        </div>

        <div class="metadata col s3 white right">
            <div class="col-content">
                <div class="input-field customer">
                    <i class="material-icons grey-text prefix" id="profile-icon">person</i>
                    <select name="customer" id="customer">
                        <!--            <option value="" disabled selected>Choose your option</option>-->
                        <option value="company1">Py Vymo</option>
                        <option value="company2">Peter</option>
                        <option value="company3">Madeleine Sowo</option>
                    </select>
                </div>
                <div class="company">
                    <i class="material-icons grey-text prefix" id="company-icon">home</i>
                    <div class="label">Py Vymo AG</div>
                </div>

                <div class="input-field agent">
                    <i class="material-icons grey-text prefix" id="profile-icon">person</i>
                    <label>Agent</label>
                    <select id="agent" name="agent">
                        <!--            <option value="" disabled selected>Choose your option</option>-->
                        <option value="company1">Daniele</option>
                        <option value="company2">Charlotte</option>
                        <option value="company3">Madeleine</option>
                    </select>
                </div>

                <div class="input-field status">
                    <label>Status</label>
                    <select name="status" id="status">
                        <!--            <option value="" disabled selected>Choose your option</option>-->
                        <option value="company1">Niedrig</option>
                        <option value="company2">Hoch</option>
                        <option value="company3">Dringend</option>
                    </select>
                </div>

                <div class="input-field priority">
                    <label>Priorität</label>
                    <select name="priority" id="priority">
                        <!--            <option value="" disabled selected>Choose your option</option>-->
                        <option value="company1">In Bearbeitung</option>
                        <option value="company2">Feedback</option>
                    </select>
                </div>

                <div class="input-field source">
                    <label>Quelle</label>
                    <select>
                        <!--            <option value="" disabled selected>Choose your option</option>-->
                        <option value="company1">Telefon Telefon Telefon</option>
                        <option value="company2">Email</option>
                    </select>
                </div>

                <div class="input-field type">
                    <label>Typ</label>
                    <select name="type" id="type">
                        <!--            <option value="" disabled selected>Choose your option</option>-->
                        <option value="company1">Telefon</option>
                        <option value="company2">Email</option>
                    </select>
                </div>

                <div class="input-field tags">
<!--                    <i class="material-icons grey-text prefix">label</i>-->
                    <label>Tags</label>
                    <select id="tags" name="tags[]" multiple="multiple">
<!--                        <option value="" disabled selected>Choose your option</option>-->
                        <option value="company1">Frage</option>
                        <option value="company2">Bug</option>
                        <option value="company3">Helleo</option>
                        <option value="company5">ekjahe</option>
                    </select>
                </div>

            </div>
        </div>

    </div>

    <div class="row ticket-item">
        <div class="col s8 white left">
            <div class="header">
                <div class="mail-receiver">
                    <div class="from">Von: py.valeris@oy-vymo.com</div>
                    <div class="cc">CC: py.valeris@oy-vymo.com</div>
                    <div class="bcc">BCC: py.valeris@oy-vymo.com</div>
                </div>
                <!-- <div class="vertical-line"></div> -->
                <div class="ticket-item-type email">
                    <!-- <div class="type"> -->
                    <i class="material-icons">email</i>
                    <!-- </div>-->
                </div>
                <!-- <div class="vertical-line"></div> -->
                <div class="answered-from-agent">
                    <div class="agent">
                        <i class="material-icons">person</i>
                        <div class="agent-name">Daniele Nicastro</div>
                    </div>
                    <div class="date">am 28.10.2018 - 17:55 Uhr</div>
                </div>
            </div>
            <div class="bottom-line"></div>
            <div class="message">
                Sehr geehrter Herr Sowieso
                <br><br>
                Gerne möchte ich auf Sie zu kommen, da es nicht ganz klar ist
                wie und was genau funktioniert.
                <br><br>

                Ein kleines Bächlein namens Duden fließt durch ihren Ort und
                versorgt sie mit den nötigen Regelialien
                <br><br>

                Beste Grüsse<br>
                Daniele
                <br><br>

                Mein Support<br>
                0487856446<br>
                Meierstrasse 99<br>
                Lässig
            </div>
        </div>
    </div>
    <div class="row ticket-item">
        <div class="col s8 white left">
            <div class="header ">
                <div class="mail-receiver">
                    <div class="from">Von: py.valeris@oy-vymo.com</div>
                </div>
                <!--                <div class="vertical-line"></div>-->
                <div class="ticket-item-type email">
                    <!--                    <div class="type ">-->
                    <i class="material-icons">email</i>
                    <!--                    </div>-->
                </div>
                <!--                <div class="vertical-line"></div>-->
                <div class="answered-from-agent">
                    <div class="agent">
                        <i class="material-icons">person</i>
                        <div class="agent-name">Daniele Nicastro</div>
                    </div>
                    <div class="date">am 28.10.2018 - 17:55 Uhr</div>
                </div>
            </div>
            <div class="bottom-line"></div>
            <div class="message">
                Sehr geehrter Herr Sowieso
                <br><br>
                Gerne möchte ich auf Sie zu kommen, da es nicht ganz klar ist
                wie und was genau funktioniert.
                <br><br>

                Ein kleines Bächlein namens Duden fließt durch ihren Ort und
                versorgt sie mit den nötigen Regelialien
                <br><br>

                Beste Grüsse<br>
                Daniele
                <br><br>

                Mein Support<br>
                0487856446<br>
                Meierstrasse 99<br>
                Lässig
            </div>
        </div>
    </div>
</div>


<?php print $bottom; ?>
<script>
    $(document).ready(function () {
        $('textarea#address, textarea#department_building_floor').characterCounter();

        // $(' select:not(#agent):not(#tags):').formSelect();
        $('select').select2();

        $('select').on('select2:open', function (e) {
            $('.select2-dropdown span:last-child').append('<span>Click me</span>')
        });


    });


</script>
<?php print $end; ?>
