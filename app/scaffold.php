<?php $top = '<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="styles/css/styles.css" media="screen,projection"/>

   

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<section class="menu container">
    <!-- Dropdown Structure -->
    <ul id="dropdown-ticket" class="dropdown-content">
        <li><a href="#!">Ticket erstellen</a></li>
        <!--    <li><a href="#!">two</a></li>-->
        <!--    <li class="divider"></li>-->
        <!--    <li><a href="#!">three</a></li>-->
    </ul>
    <ul id="dropdown-customer" class="dropdown-content">
        <li><a href="#!">Kunde hinzufügen</a></li>
        <li><a href="#!">Kunde verwalten</a></li>
        <li class="divider"></li>
        <li><a href="#!">three</a></li>
    </ul>
    <ul id="dropdown-setting" class="dropdown-content">
        <li><a href="#!">Einstellungen</a></li>
        <li><a href="#!">two</a></li>
        <li class="divider"></li>
        <li><a href="#!">three</a></li>
    </ul>
    <nav class="no-shadow">
        <div class="nav-wrapper white">
            <a href="./index.php" class="grey-text darken-4 brand-logo">Logo</a>
            <ul class="right hide-on-med-and-down">
                <li><a class="grey-font dropdown-trigger" href="#!" data-target="dropdown-ticket">Tickets<i class="material-icons right">arrow_drop_down</i></a>
                <li><a class="grey-font dropdown-trigger" href="#!" data-target="dropdown-customer">Kunden<i class="material-icons right">arrow_drop_down</i></a>
                <li><a class="grey-font" href="#!">Einstellungen</a>
                </li>
            </ul>
        </div>
    </nav>
</section>


<section class="header container overflow-hidden thin white-text">
    <div class="clock-and-name">
        <div>
            <div class="clock">ClickClack</div>
            <div class="name-date">
                <div class="name">Willkommen, Joy!</div>
                <div class="date">19.08.2018</div>
            </div>
        </div>
    </div>

    <div class="ticket-statistic">
        <div>
            <div class="left your-tickets">Deine Tickets<span>7</span></div>
            <div class="vertical-line"></div>
            <div class="right all-tickets">Alle Tickets<span>23</span></div>
        </div>
    </div>
</section>


<section class="content container base-background">';

$bottom = '
</section>
<!--JavaScript at end of body for optimized loading-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="js/bin/materialize.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<link type="text/css" rel="stylesheet" href="styles/css/select2-materialize.css" media="screen,projection"/>

<script>
    $(document).ready(function () {
        $(".dropdown-trigger").dropdown();
    });
</script>';
$end    = '</body>
</html>
';
?>
