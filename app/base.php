<?php include('scaffold.php'); ?>
<?php print $top; ?>
    <div class="elements">
        <div class="ticket-element white">
            <a class="" href="#!">
                <div class="message">
                    <div class="title"><span class="ticket-id">#525 - </span><span class="ticket-title">Neue Filteransicht benötigt für den Shop</span></div>
                    <div class="message-text ">Sehr geehrter Herr Sowieso
                        Gerne möchte ich auf Sie zu kommen, da es nicht ganz klar ist wie und was genau funktioniert.
                        Haben Sie bereits ein Auge auf die Ansicht für den Shop werden können? Es wäre gut wenn...
                    </div>
                </div>
                <div class="date-and-agent">
                    <div class="date">am 27.08.2018</div>
                    <div class="agent">Daniele Nicastro</div>
                </div>
                <div class="customer-and-company">
                    <div class="customer">Py Valeris Meier</div>
                    <div class="company">Py Vymo AG</div>
                </div>
                <div class="status-and-prio">
                    <div class="status"></div>
                    <div class="prio"></div>
                </div>
                <div class="metadata-icon"></div>
            </a>
        </div>

        <div class="ticket-element white">
            <a class="" href="#!">
                <div class="message">
                    <div class="title"><span class="ticket-id">#525 - </span><span class="ticket-title">Neue Filteransicht benötigt für den Shop</span></div>
                    <div class="message-text ">Sehr geehrter Herr Sowieso
                        Gerne möchte ich auf Sie zu kommen, da es nicht ganz klar ist wie und was genau funktioniert.
                        Haben Sie bereits ein Auge auf die Ansicht für den Shop werden können? Es wäre gut wenn...
                    </div>
                </div>
                <div class="date-and-agent">
                    <div class="date">am 27.08.2018</div>
                    <div class="agent">Daniele Nicastro</div>
                </div>
                <div class="customer-and-company">
                    <div class="customer">Py Valeris Meier</div>
                    <div class="company">Py Vymo AG</div>
                </div>
                <div class="status-and-prio">
                    <div class="status"></div>
                    <div class="prio"></div>
                </div>
                <div class="metadata-icon"></div>
            </a>
        </div>
    </div>
<?php print $bottom; ?>