<?php include('scaffold.php'); ?>
<?php print $top; ?>

<div id="customers">
    <div class="elements white">
        <div class="row">
            <div class="col l1 icon-profile">
                <i class="material-icons grey-text large" id="profile">person</i>
            </div>

            <div class="col l2 name">
                <div>Peter Mayer</div>
            </div>

            <div class="col l2 contact">
                <div>mypeter@gmail.com </div>
                <div>056 444 55 454 </div>
                <div class="mobile">0789 454 5456 56</div>
            </div>

            <div class="col l2 function">
                <div>Application Manager</div>
            </div>

            <div class="col l3 company">
                <i class="material-icons grey-text prefix" id="company-icon">home</i>
                <div class="wrapper">
                    <div class="company-name">Py Vymo AG</div>
                    <div class="company-website">www.py-vymo.com</div>
                </div>
            </div>
            <div class="col l2 permissions">
                <label>
                    <input type="checkbox" class="filled-in" checked="checked"/>
                    <span>Ticket erfassen</span>
                </label>
                <label>
                    <input type="checkbox" class="filled-in" checked="checked"/>
                    <span>Auf Ticket antworten</span>
                </label>
            </div>
        </div>
    </div>

    <div class="elements white">

        <div class="row">
            <div class="col l1 icon-profile">
                <i class="material-icons grey-text large" id="profile">person</i>
            </div>

            <div class="col l2 name">
                <div>Peter Mayer</div>
            </div>

            <div class="col l2 contact">
                <div>mypeter@gmail.com </div>
                <div>056 444 55 454 </div>
                <div class="mobile">0789 454 5456 56</div>
            </div>

            <div class="col l2 function">
                <div>Application Manager</div>
            </div>

            <div class="col l3 company">
                <i class="material-icons grey-text prefix" id="company-icon">home</i>
                <div class="wrapper">
                    <div class="company-name">Py Vymo AG</div>
                    <div class="company-website">www.py-vymo.com</div>
                </div>
            </div>
            <div class="col l2 permissions">
                <label>
                    <input type="checkbox" class="filled-in" checked="checked"/>
                    <span>Ticket erfassen</span>
                </label>
                <label>
                    <input type="checkbox" class="filled-in" checked="checked"/>
                    <span>Auf Ticket antworten</span>
                </label>
            </div>
        </div>

    </div>

    <div class="elements white">

        <div class="row">
            <div class="col l1 icon-profile">
                <i class="material-icons grey-text large" id="profile">person</i>
            </div>

            <div class="col l2 name">
                <div>Peter Mayer</div>
            </div>

            <div class="col l2 contact">
                <div>mypeter@gmail.com </div>
                <div>056 444 55 454 </div>
                <div class="mobile">0789 454 5456 56</div>
            </div>

            <div class="col l2 function">
                <div>Application Manager</div>
            </div>

            <div class="col l3 company">
                <i class="material-icons grey-text prefix" id="company-icon">home</i>
                <div class="wrapper">
                    <div class="company-name">Py Vymo AG</div>
                    <div class="company-website">www.py-vymo.com</div>
                </div>
            </div>
            <div class="col l2 permissions">
                <label>
                    <input type="checkbox" class="filled-in" checked="checked"/>
                    <span>Ticket erfassen</span>
                </label>
                <label>
                    <input type="checkbox" class="filled-in" checked="checked"/>
                    <span>Auf Ticket antworten</span>
                </label>
            </div>
        </div>

    </div>

</div>
<?php print $bottom; ?>
<script>
    $(document).ready(function () {

    });


</script>
<?php print $end; ?>
