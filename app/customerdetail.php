<?php include('scaffold.php'); ?>
<?php print $top; ?>

<div id="customerdetail" class="white">
    <form class="col s12">

        <div class="row left">
            <i class="material-icons grey-text large" id="profile">person</i>
            <div class="input-field" id="company">
                <i class="material-icons grey-text prefix" id="company-icon">home</i>
                <select>
                    <!--            <option value="" disabled selected>Choose your option</option>-->
                    <option value="company1">Py Vymo AG</option>
                    <option value="company2">Wow AG MEGA</option>
                    <option value="company3">Super AG</option>
                </select>
            </div>
            <div><a href="#">zur Firma</a></div>
        </div>

        <div class="row right">

            <div class="row">
                <div class="input-field col s12">
                    <select required>
                        <option value="" disabled selected>-- Auswählen --
                        </option>
                        <option value="male">Herr</option>
                        <option value="female">Frau</option>
                    </select>
                    <label>Anrede</label>
                </div>

                <div class="input-field col s12">
                    <select>
                        <option value="" disabled selected>-- Auswählen --
                        </option>
                        <option value="doctor">Dr.</option>
                        <option value="doctor_prof">Dr. Prof.</option>
                        <option value="prof">Professor</option>
                    </select>
                    <label>Titel</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <input id="firstname" type="text" class="validate">
                    <label for="firstname">Vorname</label>
                </div>
                <div class="input-field col s6">
                    <input id="lastname" type="text" class="validate">
                    <label for="lastname">Nachname</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="email" type="email" class="validate">
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input id="telephone" type="tel" class="validate">
                    <label for="telephone">Telefon</label>
                </div>
                <div class="input-field col s6">
                    <input id="mobile" type="tel" class="validate">
                    <label for="mobile">Mobile</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <input id="job_function" type="text" class="validate">
                    <label for="job_function">Funktion</label>
                </div>
            </div>


            <div class="row">
                <div class="input-field col s12">
                    <textarea id="department_building_floor"
                              class="materialize-textarea"
                              data-length="600"></textarea>
                    <label for="department_building_floor">Abteilung/Gebäude/Stockwerk</label>
                </div>
            </div>

            <div class="row permissions">
                <div class="input-field col s6 create-ticket">
                    <label>
                        <input type="checkbox" class="filled-in"
                               checked="checked"/>
                        <span>Ticket erfassen</span>
                    </label>
                </div>
                <div class="input-field col s6 answer-ticket">
                    <label>
                        <input type="checkbox" class="filled-in"
                               checked="checked"/>
                        <span>Auf Ticket antworten</span>
                    </label>
                </div>
            </div>

            <div class="row submit-buttons">
                <div class="input-field col s6 submit">
                    <button class="btn waves-effect waves-light" type="submit"
                            name="action">Speichern
                        <i class="material-icons left">save</i>
                    </button>
                </div>
                <div class="input-field col s6 cancel">
                    <button class="btn waves-effect waves-light" type="button"
                            name="cancel">Abbrechen
                        <i class="material-icons left">cancel</i>
                    </button>
                </div>
            </div>
        </div>
    </form>

</div>


<?php print $bottom; ?>
<script>
    $(document).ready(function () {
        $('textarea#address, textarea#department_building_floor').characterCounter();

        $('select').formSelect();

    });


</script>
<?php print $end; ?>
